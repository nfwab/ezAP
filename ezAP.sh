#!/bin/bash
# intercative script to create a custom access point with hostapd and dnsmasq
# exit properly with ctrl-c
#	note: you won't have internet accesss using this script
#	you can try to use "create_ap" ,it has LOTS of features!

#defining colors
RED='\033[0;31m'
GREEN='\033[1;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTCYAN='\033[1;36m'
YELLOW='\033[1;33m'
NC='\033[0m' #No Color

clear

echo -n -e "${LIGHTCYAN}"
cat << "EOF"
       _____________ ___________
      /__________  /\           /
     /          / /  \  XXXXX  /   
     |  XXXXXXX/ /    \  XXX  /    
     |        / /  XX  \  X  /
     |       / /        \   /
     |  XXXX/ /___XXXX___\  \
     \_____/______________\__\

EOF

echo -n -e "${NC}"

echo -e "[${YELLOW}*${NC}] ezAP has started.."
echo -e "[${YELLOW}*${NC}] Exit this script with ${YELLOW}CTRL+C${NC} and it will attempt to clean up properly"

#setting the wireless interface
if [ -z $1 ]; then
   echo -e -n "[${YELLOW}?${NC}] Wireless Interface (e.g ${GREEN}wlan0${NC}):${GREEN} "
   read iface
   echo -e -n "${NC}"
else
   iface=$1
fi

#setting the ssid of our access point
if [ -z $2 ]; then
   echo -e -n "[${YELLOW}?${NC}] SSID (e.g ${GREEN}AndroidAP${NC}) :${GREEN} "
   read ssid
   echo -e -n "${NC}"
else
   ssid=$2
fi

#setting the wireless channel of our access point
echo -e -n "[${YELLOW}?${NC}] Access Point Channel [${GREEN}1-12]${NC}:${GREEN} "
read channel
echo -e -n "${NC}"

# get wep key
function get_wep_pass() { 
	echo -e -n "[${YELLOW}?${NC}] WEP Key [must be exactly 5 or 13 ascii characters]: " 
	read wep_pass
	if [[  $wep_pass =~ ^[a-zA-Z0-9]{5}$ ]] ; then
		echo -e "${GREEN}Key accepted${NC}"
	elif [[  $wep_pass =~ ^[a-zA-Z0-9]{13}$ ]] ; then
		echo -e "${GREEN}Key accepted${NC}"
	else
		echo -e "[${RED}X${NC}] WEP key must be exactly 5 or 13 characters"
		get_wep_pass
	fi
}

# get mac
function get_mac() {
	echo -e -n "[${YELLOW}?${NC}] Enter MAC address in the following format ${GREEN}AB:CD:EF:12:34:56${NC}:${GREEN} "
	read new_mac
	echo -e -n "${NC}"
	if [[  $new_mac =~ ^[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}$ ]] ; then
		rfkill unblock wlan
		ifconfig ${iface} down
		sleep 1
		macchanger --mac=$new_mac ${iface}
		ifconfig ${iface} up
   else
		echo "[${RED}X${NC}] MAC Address format not correct."
		get_mac
	fi
}

# ask for MAC change
echo -e -n "[${YELLOW}?${NC}] Do you want to change your MAC? [y/n]: "
read changemac
case $changemac in
	y*)
		echo -e -n "[${YELLOW}?${NC}] Do you want to keep your changed MAC after? [y/n]: "
	  read keepmac
		case $keepmac in
			y*)
				#CTRL+C function
				function ctrl_c() {
					echo ""
					echo -e "[${YELLOW}*${NC}] Putting ${iface} into managed mode"
					iwconfig ${iface} mode managed
					echo -e "[${YELLOW}*${NC}] Downing ${iface}"
					ifconfig ${iface} down
					echo -e "[${YELLOW}*${NC}] Flushing iptables rules"
					iptables -F
					iptables -F -t nat
					echo -e "[${YELLOW}*${NC}] Spoofing ${iface} MAC"
					echo ""
					macchanger -a ${iface}
					echo ""
					echo -e "[${YELLOW}*${NC}] Killing dnsmasq"
					kill -9 `cat /tmp/dnsmasq.run`
					echo -e "[${YELLOW}*${NC}] Upping ${iface}"
					ifconfig ${iface} 0.0.0.0 up
				}
			;;
			n*)
				#CTRL+C function
				function ctrl_c() {
					echo ""
					echo -e "[${YELLOW}*${NC}] Putting ${iface} into managed mode"
					iwconfig ${iface} mode managed
					echo -e "[${YELLOW}*${NC}] Downing ${iface}"
					ifconfig ${iface} down
					echo -e "[${YELLOW}*${NC}] Flushing iptables rules"
					iptables -F
					iptables -F -t nat
					echo -e "[${YELLOW}*${NC}] Resetting ${iface} MAC"
					echo ""
					macchanger -p ${iface}
					echo ""
					echo -e "[${YELLOW}*${NC}] Killing dnsmasq"
					kill -9 `cat /tmp/dnsmasq.run`
					echo -e "[${YELLOW}*${NC}] Upping ${iface}"
					ifconfig ${iface} 0.0.0.0 up
				}
			;;
			*)
				echo "[${RED}X${NC}] Invalid choice, keeping current MAC address."
			;;
		esac
		
		echo -e -n "[${YELLOW}?${NC}] Custom MAC? [y/n]: "
      read random_mac
		case $random_mac in
			y*)
				get_mac
			;;
			n*)
				ifconfig ${iface} down
				echo ""
				macchanger -a ${iface}
				echo ""
				ifconfig ${iface} up
				sleep 1
			;;
			*)
				echo "[${RED}X${NC}] Invalid choice, keeping current MAC address."
			;;
		esac
	;;
    n*)
		function ctrl_c() {
			echo ""
			echo -e "[${YELLOW}*${NC}] Putting ${iface} into managed mode"
			iwconfig ${iface} mode managed
			echo -e "[${YELLOW}*${NC}] Downing ${iface}"
			ifconfig ${iface} down
			echo -e "[${YELLOW}*${NC}] Flushing iptables rules"
			iptables -F
			iptables -F -t nat
			echo -e "[${YELLOW}*${NC}] Resetting ${iface} MAC"
			echo ""
			echo ""
			echo -e "[${YELLOW}*${NC}] Killing dnsmasq"
			kill -9 `cat /tmp/dnsmasq.run`
			echo -e "[${YELLOW}*${NC}] Upping ${iface}"
			ifconfig ${iface} 0.0.0.0 up
		}
	;;
esac

# install packages if need be
if [ $(dpkg-query -W -f='${Status}' dnsmasq 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install dnsmasq
fi
if [ $(dpkg-query -W -f='${Status}' hostapd 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install hostapd
fi

# trap control c
trap ctrl_c INT

## script begins

# stop and disable services
service hostapd stop
service dnsmasq stop
pkill -9 dnsmasq
pkill -9 hostapd

# bring up ${iface}
rfkill unblock wlan
ifconfig ${iface} down
sleep 1
iwconfig ${iface} mode monitor
ifconfig ${iface} 192.168.0.1/24 up

# forwarding and nat
sysctl net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

# dnsmasq conf
cat > /tmp/dnsmasq.conf <<EOF
bind-interfaces
interface=${iface}
dhcp-range=192.168.0.2,192.168.0.255
EOF

# hostapd conf
cat > /tmp/hostapd.conf<<EOF
interface=${iface}
driver=nl80211
ssid=${ssid}
hw_mode=g
channel=${channel}
EOF

# choose AP security
echo -e -n "[${GREEN}?${NC}] Security: None,${RED}WEP${NC},${YELLOW}WPA${NC},${GREEN}WPA2${NC}  [ N;${RED}1${NC};${YELLOW}2${NC};${GREEN}3${NC} ]: "
read sec_type

###############################
case $sec_type in
n*)
echo "NONE"
;;
N*)
echo "NONE"
;;
1*)
#########
#  WEP  #
#########
echo ""
echo -e "${RED}[ WEP ] ${NC}"
echo ""
# asking for WEP password
get_wep_pass
echo -e "[${YELLOW}*${NC}] writing to hostapd.conf"
cat >>/tmp/hostapd.conf <<EOF
wep_default_key=0
wep_key="${wep_pass}"
EOF
echo -e "[${YELLOW}*${NC}] done."
;;
2*)
#########
#  WPA  #
#########
echo ""
echo -e "${YELLOW}[ WPA ] ${NC}"
echo ""
# asking for WPA password
echo -e -n "[${YELLOW}?${NC}] Choose WPA password: "
read wpa_pass
echo -e "[${YELLOW}*${NC}] writing to hostapd.conf"
cat >>/tmp/hostapd.conf <<EOF
wpa=1
wpa_passphrase=${wpa_pass}
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
EOF
echo -e "[${YELLOW}*${NC}] done."
;;
3*)
##########
#  WPA2  #
##########
echo ""
echo -e "${GREEN}[ WPA2 ] ${NC}"
echo ""
# asking for WPA2 password
echo -e -n "[${YELLOW}?${NC}] Choose WPA2 password: "
read wpa2_pass
echo -e "[${YELLOW}*${NC}] writing to hostapd.conf"
cat >>/tmp/hostapd.conf <<EOF
wpa=2
wpa_passphrase=${wpa2_pass}
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
EOF
echo -e "[${YELLOW}*${NC}] done."
;;
*)
echo -e "[${RED}X${NC}] Invalid choise, try again."
;;
esac

###############################


# if WEP key, add to hostapd conf
#if [[ -n $wep_pass ]]; then echo -e "wep_default_key=0\nwep_pass0=\"${wep_pass}\"" >> /tmp/hostapd.conf; fi

#kill proccesses
sudo service network-manager stop
sudo airmon-ng check kill
echo -e "[${YELLOW}*${NC}] catching some zZzzz-es"
sleep 2

# run dnsmasq and hostapd
dnsmasq --pid-file=/tmp/dnsmasq.run -C /tmp/dnsmasq.conf
hostapd /tmp/hostapd.conf
